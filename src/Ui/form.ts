import { ERROR_COLOR } from "./../shared/style";
import styled from "styled-components";

const GREY_DEFAULT: string = "#859090";
export const HorizontalForm = styled.form.attrs({
  className: "form-horizontal"
})`
  min-height: 65vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: stretch;
`;
export const FormGroup = styled.div.attrs({})<{ isValid: boolean }>`
  margin: 10px 0;
  grid-template-columns: 25% auto;
  display: grid;
  .form-input {
    color: ${GREY_DEFAULT};
    border-bottom: ${props =>
      `1px solid ${props.isValid ? "#bcc3ce" : ERROR_COLOR}`};
  }
`;
export const FormLabel = styled.label.attrs({
  className: "form-label"
})<{ htmlFor: string }>`
  display: flex;
  align-items: center;
  font-size: 14px;
  color: ${GREY_DEFAULT};
`;

export const FormHint = styled.p`
  color: ${ERROR_COLOR};
  font-size: 0.7rem;
  padding-top: 0.2rem;
  margin-bottom: 0;
  min-height: 20px;
  width: 100%;
  white-space: nowrap;
`;
