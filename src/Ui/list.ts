import styled from "styled-components";

export const ListLink = styled.a.attrs({ className: "hookia__cell" })``;

export const ListText = styled.p.attrs({
  className: "hookia__cell ReactVirtualized__Table__headerTruncatedText"
})``;

export const ListImage = styled.img.attrs({ className: "hookia__cell" })``;

export const ListCode = styled.code.attrs({ className: "hookia__cell" })``;
export const Row = styled.div``;

export const ColumnListTitle = styled.p`
  color: #6500fc;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 3px;
`;

export const TableDDContainer = styled.div``;
