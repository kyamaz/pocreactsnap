import styled from "styled-components";

export const DragHandle = styled.span`
  width: 2px;
  background-color: #8414f7;
  transform: none !important;
  &:hover {
    cursor: pointer;
  }
`;
