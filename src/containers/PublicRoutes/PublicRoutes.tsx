import * as React from "react";
import { useState } from "react";
//style

import { DEFAULT_COLOR } from "@shared/style";
//components
import { LoginForm } from "@components/LoginForm/LoginForm";
import { VerticalColumns, LoginHeroBg, LoginTopBar } from "@ui/layout";
import { InlineBtn, SignInBtn } from "@ui/btn";
import { Tabs, TabItem } from "@ui/tabs";
import { TransStr } from "@components/TransStr/TransStr";
//hooks
import { useToggle } from "@hooks/useToggle";
import { useTryAuth, useValidationError } from "@hooks/useTryLogin";
import { Modal } from "@components/Modal/Modal";
//Model
import {
  LoginType,
  AuthPayload,
  UseLoginType,
  UseAuthPayload
} from "@model/index";
import { Route } from "react-router";
import { PublicWrap } from "@components/PublicWrap/PublicWrap";



function PublicRoutes({ component, ...rest }): JSX.Element {
 
  return (
    <Route
      {...rest}
      render={matchProps => (
       <PublicWrap      component={component}    />
      )}
    />
  );

}
export default PublicRoutes;
