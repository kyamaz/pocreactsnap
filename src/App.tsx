import * as React from "react";
import { Fragment } from "react";

//style
import { GlobalStyle } from "@shared/style";
//navigation
import {  Switch } from "react-router-dom";
//routes
import { AppContainer, LoginContainer } from "./routes";
import AboutContainer from "@containers/About/About";

import ProtectedRoute from "./containers/ProtectedRoutes/ProtectedRoutes";
//translation
import { IntlProvider } from "react-intl";
import { useLocalLang } from "@hooks/useLocalLang";
import PublicRoutes from "@containers/PublicRoutes/PublicRoutes";


//use key for dinamic language selection https://github.com/yahoo/react-intl/wiki/Components
function AppWrap(): JSX.Element {
  const browserLang = navigator.language.split(/[-_]/)[0]; // language without region code
  const [locale, localData] = useLocalLang(browserLang);

  return (
    <IntlProvider locale={locale} key={locale} messages={localData}>
      <Fragment>
        <GlobalStyle />
        <Switch>
          <PublicRoutes path="/login" component={LoginContainer} />
          <PublicRoutes path="/about" component={AboutContainer} />
          <ProtectedRoute path="/" exact component={AppContainer} />
          <ProtectedRoute component={AppContainer} />
        </Switch>
      </Fragment>
    </IntlProvider>
  );
}

export default AppWrap;
