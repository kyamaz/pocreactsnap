import * as React from "react";
import { hydrate, render } from "react-dom";
import registerServiceWorker from "./registerServiceWorker";
import { unregister } from "./registerServiceWorker";

//css library

import "./styles.css";
import AppWrap from "./App";
import { Router } from "react-router-dom";
import { history } from "./route-history";

import { loadComponents } from "loadable-components";
import { getState } from "loadable-components";
window["snapSaveState"] = () => getState();

const rootElement = document.getElementById("root");

const AppWithRouter = (
  <Router history={history}>
    <AppWrap />
  </Router>
);

//render(<AppWrap />, document.getElementById("root") as HTMLElement);
if (rootElement.hasChildNodes()) {
  loadComponents()
    .then(() => hydrate(AppWithRouter, rootElement))
    .catch(() => render(AppWithRouter, rootElement));
} else {
  render(AppWithRouter, rootElement);
}

//registerServiceWorker();
//unregister service worker for now as it always redirect to login first-> not updated correctly
//TODO handle cache validation
unregister();
