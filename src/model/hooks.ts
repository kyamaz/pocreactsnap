import { AuthPayload } from "@model/index";
import { LoginType, FormsControlsRef, InputState } from "./form";
import { SetStateAction, Dispatch } from "react";

export type UseDispatch<T extends Object> = Dispatch<SetStateAction<T>>;
export type UseHooks<T extends Object> = [T, UseDispatch<T>];
export type UseDispatchState<T extends Object> = Dispatch<SetStateAction<T>>;

export type UseBoolean = UseHooks<boolean>;
export type UseLoginType = UseHooks<LoginType>;
export type UseAuthPayload = UseHooks<AuthPayload>;

// custom hooks
export interface UseLoginHook {
  loginInput: InputState;
  pwdInput: InputState;
  confirmInput: InputState;
  isFormValid: boolean;
  inputsRef: FormsControlsRef;
  updateFormState: Function;
}
