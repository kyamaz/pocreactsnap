import { DispatchAction } from "@model/index";

export type ValidPayload = string | number | boolean | Array<any> | Object;

export interface DispatchAction<T extends ValidPayload> {
  type: string;
  payload: T;
}

export interface StoreState {
  //  list: StoreListState;
}
export interface Store {
  getState: Function;
  dispatch: Function;
}

export type Reducer = (
  state: StoreState,
  action: DispatchAction<ValidPayload>
) => StoreState;

export type DispatchToStore<T> = (dispatch: DispatchAction<T>) => void;
