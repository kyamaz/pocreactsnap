//Constants
//URLS
//export const API: string = "http://localhost:4500/"; //dev
//export const API: string = "http://localhost:8080/"; //dev
export const API: string = "https://hookia.herokuapp.com/"; //prod
export const LOGIN_ENDPOINT = `${API}login`;
export const TV_ENDPOINT = `${API}tv`;
export const TV_SELECTION_ENDPOINT = `${API}tv-selection`;
