import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
html{
  height:100vh;
}
#root, body{
  height:inherit;
}
#root{
  display:flex;
  flex-direction:column;
}`;

//Constant
export const DEFAULT_COLOR: string = "#5755d9";
export const ERROR_COLOR: string = "#e85600";

export const BG_BASE = `linear-gradient(
  153deg,
  #5100cb 0,
  #6500fc 42%,
  #9617c9 75%,
  #d33092 100%
)
right bottom no-repeat`;
