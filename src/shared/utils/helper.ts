export function safeGet<T extends Object>(value: Object, ...path: string[]): T {
  return path.reduce((prev: Object, prop: string) => {
    if (prev && !!prev[prop]) {
      return prev[prop];
    } else {
      return null;
    }
  }, value);
}

export const normalizeStr = (str: string): string =>
  str
    .replace(/\s+/g, "")
    .trim()
    .toUpperCase();

//forms

export const isEmpty = (val: string): boolean =>
  val ? val.trim() === "" : true;
export const isValidUrl = (u: string): boolean =>
  u
    ? !!u.match(
        /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
      )
    : false;

export function isEquivalent(a: Object, b: Object): boolean {
  // Create arrays of property names
  const aProps = Object.getOwnPropertyNames(a);
  const bProps = Object.getOwnPropertyNames(b);

  // If number of properties is different,
  // objects are not equivalent
  if (aProps.length != bProps.length) {
    return false;
  }

  for (var i = 0; i < aProps.length; i++) {
    const propName = aProps[i];

    // If values of same property are not equal,
    // objects are not equivalent
    if (a[propName] !== b[propName]) {
      return false;
    }
  }

  // If we made it this far, objects
  // are considered equivalent
  return true;
}

export function IsArrayObjectEqual(sourceArr, newArr): boolean {
  // Get the value type
  const type = Object.prototype.toString.call(sourceArr);
  // If the two objects are not the same type, return false
  if (type !== Object.prototype.toString.call(newArr)) return false;
  // If items are not an object or array, return false
  if (["[object Array]", "[object Object]"].indexOf(type) < 0) return false;
  if (sourceArr.length !== newArr.length) return false;
  const equals = sourceArr.reduce(
    (accu, curr) => newArr.filter(n => n === curr).length > 0,
    true
  );
  return equals;
}
