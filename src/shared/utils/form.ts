import { AuthPayload } from "@model/index";
import {
  FormControls,
  FormsControlsRef,
  InputState,
  FormsControlState,
  ValidateState,
  ErrorMessagesMap,
  ValidationKey
} from "./../../model/form";

export const ERROR_MSG: ErrorMessagesMap = {
  required: "this field is required",
  minLength: "this field is too short",
  isEqual: "Fields are not identicale",
  isUrl: "Not a valid url "
};
/**
 *
 * Form UI
 */

/**
 *
 * display error msg
 * @export
 * @param {FormsControlState} input
 * @returns {string}
 */
export function displayErrorHint(input: FormsControlState): string {
  const {
    state: { errorDisplay }
  } = input;
  return ERROR_MSG[errorDisplay];
}
/**
 * display login submit result.
 *  Default value is true to initial invalid form state
 * @export
 * @param {FormsControlState} fc
 * @returns {boolean}
 */
export function uiValidation(fc: FormsControlState): boolean {
  return fc.state.touched ? fc.state.valid : true;
}
/**
 *  Form validation
 */
/**
 * Apply validation rules to control
 *
 * @export
 * @param {*} val
 * @param {Array<ValidationKey>} rules
 * @returns {ValidateState}
 */
export function validate(val: any, rules: Array<ValidationKey>): ValidateState {
  if (!Array.isArray(rules)) {
    return null;
  }
  return rules.reduce(
    (accu: ValidateState, curr: string) => {
      if (curr === "required") {
        return validationPayload(notEmpty(val), "required");
      }
      if (curr === "minLength") {
        return validationPayload(val.length > 5, "minLength");
      }
      if (curr === "isUrl") {
        return validationPayload(isValidUrl(val), "isUrl");
      }
      return accu;
    },
    { value: true, message: null }
  );
}
/**
 * apply form control validation
 *
 * @param {boolean} value
 * @param {string} message
 * @returns {ValidateState}
 */
function validationPayload(
  value: boolean,
  message: ValidationKey
): ValidateState {
  return {
    value,
    message
  };
}
/**
 *
 *
 * @param {string} val
 * @returns
 */
function notEmpty(val: string) {
  return val.trim() !== "";
}
/**
 *
 *
 * @param {string} url
 * @returns {boolean}
 */
function isValidUrl(url: string): boolean {
  return !!url.match(
    /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
  );
}
/**
 * set form valid state
 *
 * @export
 * @param {FormControls} ctrls
 * @returns {boolean}
 */
export function controlsAreValid(ctrls: FormControls): boolean {
  return Object.keys(ctrls).reduce((accu: boolean, curr: string): boolean => {
    if (!ctrls[curr].hasOwnProperty("valid")) {
      return accu;
    }
    //toggle bool as return invalid state
    return accu && ctrls[curr].valid;
  }, true);
}
/**
 *
 * Form model
 */

/**
 *
 *format inputs into FormsControlsRef
 * @export
 * @param {Array<[string, InputState, Function, Array<ValidationKey>]>} inputs
 * @returns
 */
export function makeFormState(
  inputs: Array<[string, InputState, Function, Array<ValidationKey>]>
): FormsControlsRef {
  return inputs.reduce((acc, curr) => {
    const [key, inputState, inputStateSetter, inputValidations] = curr;
    const state = {
      [key]: {
        state: inputState,
        setter: inputStateSetter,
        validations: inputValidations
      }
    };
    return {
      ...acc,
      ...state
    };
  }, {});
}
/**
 *
 *
 * @export
 * @param {FormsControlsRef} form
 * @returns {AuthPayload}
 */
export function formsCtrlsToPayload(form: FormsControlsRef): AuthPayload {
  const formVal = formStateToValue(form);
  const { loginInput, pwdInput } = formVal;
  return {
    login: loginInput,
    pwd: pwdInput
  };
}

/**
 * format forms controls model to payload auth model
 *
 * @export
 * @param {FormsControlsRef} ctrls
 * @returns {{ loginInput?: string; pwdInput?: string }}
 */
export function formStateToValue(
  ctrls: FormsControlsRef
): { loginInput?: string; pwdInput?: string } {
  const { confirmInput, ...form } = ctrls;
  return Object.keys(form).reduce(
    (acc: { loginIput: string; pwdInput: string }, curr: string) => ({
      ...acc,
      ...{ [curr]: ctrls[curr].state.value }
    }),
    {}
  );
}
const INITIAL_STATE: InputState = {
  value: "",
  valid: false,
  touched: false,
  errorDisplay: null
};
/* const loginInitialState: InputState = {
  ...inputInitialState,
  validationType: REQUIRED_VAL
};
const pwInitialState: InputState = {
  ...inputInitialState,
  validationType: ["required", "minLength"]
};
const confirmInitialState: InputState = {
  ...inputInitialState,
  validationType: []
}; */

export function formatInputState(
  validationType: Array<ValidationKey>,
  state = INITIAL_STATE
): InputState {
  return {
    ...state,
    validationType
  };
}
