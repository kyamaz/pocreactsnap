import * as React from "react";
import { useState } from "react";
//style
import { DEFAULT_COLOR } from "@shared/style";
//components
import { LoginForm } from "@components/LoginForm/LoginForm";
import { VerticalColumns, LoginHeroBg, LoginTopBar } from "@ui/layout";
import {  SignInBtn } from "@ui/btn";
import { Tabs, TabItem } from "@ui/tabs";
import { TransStr } from "@components/TransStr/TransStr";
//hooks
import { useTryAuth, useValidationError } from "@hooks/useTryLogin";
import { Modal } from "@components/Modal/Modal";
//Model
import {
  LoginType,
  AuthPayload,
  UseLoginType,
  UseAuthPayload
} from "@model/index";

import { PublicNavBar } from "@components/PublicNavBar/PublicNavBar";

interface LoginProps {
  component: any;
}
function publicWrap(props: LoginProps): JSX.Element {
  const { component: Component } = props;
  const [toggleForm, handleUiToogle] = useState(false);
  //state signin type
  const [loginType, setLoginType]: UseLoginType = useState<LoginType>("signin");
  //user try auth map
  const [tryAuthState, setTryAuthState]: UseAuthPayload = useState<AuthPayload>(
    null
  );
  const [tryAuth] = useTryAuth(tryAuthState);
  //reset validation error  signin type
  const [formTouched, setFormTouched] = useValidationError();

  const handleLoginType: (type: LoginType) => void = type => {
    handleResetValidationError();
    setLoginType(type);
  };
/*   const handlePane: (a: LoginType) => void = type => {
    handleUiToogle();
    handleLoginType(type);
  }; */
  const handleResetValidationError: () => void = () => setFormTouched(false);
  const handleTryAuth: (f: AuthPayload) => void = form => {
    setTryAuthState(form), setFormTouched(true);
  };
  const onHandleUiChange=(state)=>handleUiToogle(state)

  const onHandlePaneChange=type=>handleLoginType(type)
  return (
    <VerticalColumns>
      <LoginHeroBg isExpanded={toggleForm}>
      <PublicNavBar  uiState={toggleForm} handleUiChange={onHandleUiChange} handlePaneChange={onHandlePaneChange}/>
{/*         <LoginTopBar>
          <InlineBtn>
            {PUBLIC_ROUTES.map((route, i) => (
              <Route
              key={`route__${i}`}

                path={route.path}
                render={props => <route.component {...props} />}
                children={({ match }) => <Link to={route.path}> { route.title }</Link>}
              />
            ))}

            <SignInBtn
              data-testid="signin-btn"
              id="signin-btn"
              onClick={() => handlePane("signin")}
            >
              <TransStr id={"login.sign_in"} />
            </SignInBtn>
            <SignInBtn
              data-testid="signup-btn"
              id="signup-btn"
              onClick={() => handlePane("signup")}
            >
              <TransStr id={"login.sign_up"} />
            </SignInBtn>
          </InlineBtn>
        </LoginTopBar> */}
        <Modal
          id="login-modal"
          isExpanded={toggleForm}
          onCloseModal={handleUiToogle}
          width="33%"
        >
          <div>
            <Tabs>
              <TabItem>
                <SignInBtn
                  id="signin-tab"
                  isActive={loginType === "signin"}
                  color={DEFAULT_COLOR}
                  onClick={() => handleLoginType("signin")}
                  style={{ paddingBottom: "15px" }}
                >
                  <TransStr id={"login.sign_in"} />
                </SignInBtn>
              </TabItem>
              <TabItem>
                <SignInBtn
                  id="signup-tab"
                  isActive={loginType === "signup"}
                  color={DEFAULT_COLOR}
                  onClick={() => handleLoginType("signup")}
                  style={{ paddingBottom: "15px" }}
                >
                  <TransStr id={"login.sign_up"} />
                </SignInBtn>
              </TabItem>
            </Tabs>
            <LoginForm
              id="login-form"
              loginType={loginType}
              tryAuth={handleTryAuth}
              resetValidationError={handleResetValidationError}
              validState={formTouched ? tryAuth.isSuccess : true}
            />
          </div>
        </Modal>
        <Component />
      </LoginHeroBg>
    </VerticalColumns>
  );
}
export { publicWrap as PublicWrap };
