import * as React from "react";
import {
  render,
  fireEvent,
  cleanup,
  getByTestId,
  waitForElement
} from "react-testing-library";
import { LoginForm } from "@components/LoginForm/LoginForm";
import { testIdGetter } from "@mock/mock.utils";
import { ERROR_MSG } from "@shared/utils/form";
import userEvent from "user-event";

describe("Fill form's input", () => {
  // automatically unmount and cleanup DOM after the test is finished.
  afterEach(cleanup);

  test("It should display login error", () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();
    let isInvalid = false;
    const { getByTestId } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
      />
    );

    const { loginInput } = testIdGetter<HTMLInputElement>(
      ["loginInput", "login-input"],
      getByTestId
    );
    fireEvent.change(loginInput, { target: { value: "test" } });
    fireEvent.change(loginInput, { target: { value: null } });
    const { inputError } = testIdGetter<HTMLInputElement>(
      ["inputError", "input-error"],
      getByTestId
    );
    expect(inputError.textContent).toBe(ERROR_MSG.required);
  });

  test("It should display password error", () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();
    let isInvalid = false;
    const { getByTestId, rerender } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
      />
    );
    const MOCK_PWD: string = "re";
    const { pwdInput } = testIdGetter<HTMLInputElement>(
      ["pwdInput", "pwd-input"],
      getByTestId
    );
    fireEvent.change(pwdInput, { target: { value: MOCK_PWD } });
    expect(pwdInput.value).toBe(MOCK_PWD);
    const { pwdError } = testIdGetter<HTMLInputElement>(
      ["pwdError", "pwd-error"],
      getByTestId
    );
    expect(pwdError.textContent).toBe(ERROR_MSG.minLength);
  });
  test("It should match input", () => {
    const handleTryAuth = jest.fn();
    const loginType = "signin";
    const handleResetValidationError = jest.fn();
    let isInvalid = false;
    const { getByTestId } = render(
      <LoginForm
        id="login-form"
        loginType={loginType}
        tryAuth={handleTryAuth}
        resetValidationError={handleResetValidationError}
        validState={isInvalid}
      />
    );
    const LOGIN_INPUT: string = "toto";
    const { loginInput } = testIdGetter<HTMLInputElement>(
      ["loginInput", "login-input"],
      getByTestId
    );
    fireEvent.change(loginInput, { target: { value: LOGIN_INPUT } });
    expect(loginInput.value).toBe(LOGIN_INPUT);


    const MOCK_PWD: string = "rerere";
    const { pwdInput } = testIdGetter<HTMLInputElement>(
      ["pwdInput", "pwd-input"],
      getByTestId
    );
    fireEvent.change(pwdInput, { target: { value: MOCK_PWD } });
    expect(pwdInput.value).toBe(MOCK_PWD);
  
  });


});
