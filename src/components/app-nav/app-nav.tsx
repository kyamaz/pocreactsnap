import * as React from "react";

import { NavHeader, NavSection, NavLinkItem } from "@ui/navbar";

function appNav(props: any): JSX.Element {
  return (
    <NavHeader>
      <NavSection data-testid="links">
        <NavLinkItem exact={true} to="/" data-testid="link-home">
          home
        </NavLinkItem>
      </NavSection>
    </NavHeader>
  );
}

export { appNav as AppNav };
