import * as React from "react";
import {createMemorySource, LocationProvider, createHistory} from '@reach/router'
import { cleanup, render } from "react-testing-library";
import App from "@containers/App/App";
import { testIdGetter } from "@mock/mock.utils";

afterEach(cleanup)


function renderWithRouter(
    ui,
    {route = '/', history = createHistory(createMemorySource(route))} = {},
  ) {
    return {
      ...render(<LocationProvider history={history}>{ui}</LocationProvider>),
      // adding `history` to the returned utilities to allow us
      // to reference it in our tests (just try to avoid using
      // this to test implementation details).
      history,
    }
  }



function mockHandle(data) {
  data;
}
describe("Container should math route", () => {
  beforeEach(function() {
    // import and pass your custom axios instance to this method
  });

  afterEach(function() {
    // import and pass your custom axios instance to this method
    cleanup();
  });

  test("It should display the main page by default", async () => {
   
    const {
        container, 
        history  : {navigate},
        getByTestId
      } = renderWithRouter(<App />)
      const { appTitle } = testIdGetter(
        ["appTitle", "app-title"],
        getByTestId
      );
        expect(appTitle.textContent).toBe('Stateless simple react hook starter')
      
        await navigate('/list')
/*         const { listTitle } = testIdGetter(
            ["listTitle", "list-title"],
            getByTestId
          );

          expect(listTitle.textContent).toBe('Dynamic list sample')
 */
  });
 


});
