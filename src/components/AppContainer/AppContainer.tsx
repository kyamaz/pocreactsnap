import * as React from "react";
import { Route } from "react-router-dom";
import { AppNav } from "@components/app-nav/app-nav";
import { ListWraper } from "@ui/layout";

const appContainer = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={matchProps => (
        <ListWraper>
          <AppNav />
            <Component {...matchProps} />
        </ListWraper>
      )}
    />
  );
};

export {appContainer as AppContainer}