import * as React from "react";
import {FormattedMessage } from 'react-intl';
interface TransProps {
  id: string,
  description?: string | object,
}
function transStr (props: TransProps): JSX.Element {
  const {id, description}=props
  return(
    <FormattedMessage id={id}  defaultMessage="no transalation yet" description={description}>
      {(txt:string) => txt}</FormattedMessage>
   )
}


export { transStr as TransStr };
