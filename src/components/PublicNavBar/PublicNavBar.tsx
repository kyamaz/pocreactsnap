import * as React from "react";
import { useState } from "react";

import * as PropTypes from "prop-types";

//component
import { LoginTopBar } from "@ui/layout";
import { InlineBtn, SignInBtn } from "@ui/btn";
import { TransStr } from "@components/TransStr/TransStr";
import { PUBLIC_ROUTES } from "src/routes";
import { Route } from "react-router";

import {StyledLink} from '@ui/link'

//model
import {

  LoginType,
} from "@model/index";


function publicNavBar(props) {
const {handleUiChange, uiState, handlePaneChange}=props
  const [toggleForm, handleUiToogle] = useState(uiState);
    //update state on inner change
  React.useEffect(
    ()=>{
        handleUiChange(toggleForm)
    }
  ,[toggleForm ])

  //update state on close modal from parent
   React.useEffect(
    ()=>{
      
        handleUiToogle(uiState);
    }
  ,[uiState ]) 


  const handlePane: (a: LoginType) => void = type => {
    handleUiToogle(!toggleForm);
    //handleLoginType(type);
    handlePaneChange(type)
  };
  return (
    <LoginTopBar>
      {PUBLIC_ROUTES.map((route, i) => (
        <Route
          key={`route__${i}`}
          path={route.path}
          render={props => <route.component {...props} />}
          children={({ match }) => <StyledLink   to={route.path}> {route.title}</StyledLink>}
        />
      ))}
      <InlineBtn>
        <SignInBtn
          data-testid="signin-btn"
          id="signin-btn"
          onClick={() => handlePane("signin")}
        >
          <TransStr id={"login.sign_in"} />
        </SignInBtn>
        <SignInBtn
          data-testid="signup-btn"
          id="signup-btn"
          onClick={() => handlePane("signup")}
        >
          <TransStr id={"login.sign_up"} />
        </SignInBtn>
      </InlineBtn>
    </LoginTopBar>
  );
}

publicNavBar.propTypes = {
    uiState:PropTypes.bool,
    handleUiChange: PropTypes.func,
    handlePaneChange: PropTypes.func

};

export { publicNavBar as PublicNavBar };
