import loadable from "loadable-components";
import AboutContainer from "@containers/About/About";
export const AppContainer = loadable(() => import("./containers/App/App"), {
  LoadingComponent: () => null
});

export const LoginContainer = loadable(
  () => import("./containers/Login/Login"),
  {
    LoadingComponent: () => null
  }
);
/* export const AboutContainer = loadable(() =>  import("./containers/About/About"), {
  LoadingComponent: () => null
}); */

export const PUBLIC_ROUTES: Array<any> = [
  {
    path: "/login",
    component: LoginContainer,
    title: "Login"
  },
  {
    path: "/about",
    component: AboutContainer,
    title: "About"
  }
];
