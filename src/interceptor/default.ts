import axios from "axios";
import { API } from "@shared/const";

let instance = axios.create({
  baseURL: API,
  headers: {
    Accept: "application/json;",
    "Content-Type": "application/json;charset=utf-8",
    "Access-Control-Allow-Origin": "*"
  }
});

instance.interceptors.request.use(
  config => {
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error.response);
  }
);

// Add a response interceptor
instance.interceptors.response.use(
  response => {
    // Do something with response data
    return response;
  },
  error => {
    // Do something with response error
    return Promise.reject(error.response);
  }
);

export { instance as DEFAULT_API };
