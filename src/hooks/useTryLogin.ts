import { ApiAuthSuccess, BaseApiResponse } from "./../model/api";
import { LOGIN_ENDPOINT } from "./../shared/const";
import { AxiosResponse } from "axios";
import { AuthState } from "./useTryLogin";
import { useState, useEffect } from "react";
import { history } from "./../route-history";
import { UseBoolean } from "@model/index";
import { AppToken } from "../state/state";
import { DEFAULT_API } from "@interceptor/default";
import { ApiAuthResponse } from "@model/api";
interface AuthPayload {
  login: string;
  pwd: string;
}
export interface AuthState {
  isSuccess: boolean;
  payload: string;
}

const authApi = (
  payload: AuthPayload
): Promise<BaseApiResponse<ApiAuthResponse>> => {
  return DEFAULT_API.post(LOGIN_ENDPOINT, payload)
    .then(
      (response: AxiosResponse<BaseApiResponse<ApiAuthSuccess>>) =>
        response.data
    )
    .catch(error => Promise.reject(error.data));
};
export const useTryAuth = (payload: AuthPayload): [AuthState] => {
  const [tryAuth, setTryAuth] = useState({ isSuccess: false, payload: null });
  useEffect(
    () => {
      if (!!payload) {
        authApi(payload)
          .then((response: BaseApiResponse<ApiAuthSuccess>) => {
            const { token } = response;
            setTryAuth({ isSuccess: true, payload: token });
            AppToken.set(token);
            return token;
          })
          .then((token: string) => localStorage.setItem("hookia_token", token))
          .then(res => history.push("/"))
          .catch(err => setTryAuth({ isSuccess: false, payload: err }));
      }
    },
    [payload]
  );
  return [tryAuth];
};
export const useValidationError = (touchState: boolean = false): UseBoolean => {
  const [formTouched, setFormTouched]: UseBoolean = useState<boolean>(
    touchState
  );
  useEffect(
    () => {
      setFormTouched(touchState);
    },
    [touchState]
  );
  return [formTouched, setFormTouched];
};
