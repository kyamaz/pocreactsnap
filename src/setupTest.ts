import axios from "axios";
// react-testing-library renders your components to document.body,
// this will ensure they're removed after each test.
import "react-testing-library/cleanup-after-each";
// this adds jest-dom's custom assertions
import "jest-dom/extend-expect";
import { GlobalWithFetchMock } from "jest-fetch-mock";

require("jest-localstorage-mock");
const customGlobal: GlobalWithFetchMock = global as GlobalWithFetchMock;
customGlobal.fetch = require("jest-fetch-mock");
customGlobal.fetchMock = customGlobal.fetch;

export const mockPost = async (fn: Function, endpoint, payload) => {
  return axios.post(endpoint, payload).then(r => console.log(r));
  /* try {
    const result = await axios.post(endpoint, payload);
    return () => {
      return result;
    };
  } catch (e) {
    return () => {
      console.error(e);
      throw e;
    };
  } */
};
