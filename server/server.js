var express = require("express");
var bodyParser = require("body-parser");
var http = require("http");
var path = require("path");
var session = require("express-session");
var cors = require("cors");
var Server = (function () {
    function Server() {
        this.secret = "secretsecret";
        this.http = http;
        this.options = {};
        // Create expressjs application
        this.app = express();
        // Configure application
        this.config();
        // Setup routes
        this.routes();
        // Create server
        this.server = this.http.createServer(this.app);
        // Start listening
        this.listen();
    }
    // Bootstrap the application.
    Server.bootstrap = function () {
        return new Server();
    };
    // Configuration
    Server.prototype.config = function () {
        // By default the port should be 8080
        this.port = parseInt(process.env.PORT) || 4500;
        //cors
        this.app.use(cors());
        // Initialize Passport
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use(session({ secret: "adminAdmin", resave: false, saveUninitialized: false }));
    };
    // Configure routes
    Server.prototype.routes = function () {
        var DIST_FOLDER = path.join(process.cwd(), "build");
        this.app.set("view engine", "html");
        this.app.set("views", DIST_FOLDER);
        this.app.use(express.static(DIST_FOLDER));
        this.app.use("/", require("./routes/api.js"));
    };
    // Start HTTP server listening
    Server.prototype.listen = function () {
        var _this = this;
        //listen on provided ports
        this.server.listen(this.port);
        //add error handler
        this.server.on("error", function (error) {
            console.log("ERROR", error);
        });
        //start listening on port
        this.server.on("listening", function () {
            console.log("==> Listening: Open up http://localhost:%s/ in your browser.", _this.port);
        });
    };
    return Server;
}());
function returnEnv() {
    return process.env.NODE_ENV;
}
// Bootstrap the server
Server.bootstrap();
module.exports = Server;
