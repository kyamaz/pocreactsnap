var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var _express = require("express");
var _path = require("path");
var join = _path.join;
var DIST_FOLDER = join(process.cwd(), "build");
var router = _express.Router();
var fixtureTv = require("../fixture/tv-data.json");
var fixtureSelection = require("../fixture/tv.selection.js");
//auth
router.post("/login", function (req, res) {
    var _a = req.body, login = _a.login, pwd = _a.pwd;
    if (login === "toto" && pwd === "rerere") {
        var nonce = Math.random().toString(36);
        return res.status(200).json({ token: nonce });
    }
    return res.status(401).json({ message: "invalid creds" });
});
// Server static files from /browser
router.get("/tv", function (req, res) {
    var ApiResponse = __assign({}, fixtureTv, { status: 200 });
    return res.status(200).json(ApiResponse);
});
router.get("/tv-selection", function (req, res) {
    var ApiResponse = __assign({ data: fixtureSelection }, { status: 200 });
    return res.status(200).json(ApiResponse);
});
router.get("*", function (req, res) {
    res.sendFile(join(DIST_FOLDER, "./index.html"));
});
module.exports = router;
