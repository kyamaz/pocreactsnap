/// <reference types="cypress" />

describe("tv endpoint", () => {
  it("should return a collection of dara", () => {
    // https://on.cypress.io/server

    cy.server().should(server => {
      // the default options on server
      // you can override any of these options
      expect(server.delay).to.eq(0);
      expect(server.method).to.eq("GET");
      expect(server.status).to.eq(200);
      // These options control the server behavior
      // affecting all requests

      // pass false to disable existing route stubs
      expect(server.enable).to.be.true;
      // forces requests that don't match your routes to 404
      expect(server.force404).to.be.false;
      // whitelists requests from ever being logged or stubbed
      expect(server.whitelist).to.be.a("function");
    });

    cy.server({
      method: "POST",
      delay: 1000,
      status: 422,
      response: {}
    });

    // any route commands will now inherit the above options
    // from the server. anything we pass specifically
    // to route will override the defaults though.
  });

  it("cy.request() - make an XHR request", () => {
    // https://on.cypress.io/request
    cy.request("https://hookia.herokuapp.com/tv").should(response => {
      expect(response.status).to.eq(200);
      expect(response.body).to.have.property("data");
      expect(response.body.data).length.to.greaterThan(0);
      expect(response).to.have.property("headers");
      expect(response).to.have.property("duration");
    });
  });
});
describe("landing page list", () => {
  before(function() {
    cy.visit("/login");

    //@ts-ignore
    cy.login();
    //
  });

  it("should display values", () => {
    cy.visit("/list");

    cy.get("h1[data-testid='list-title']").should($title =>
      expect($title.text()).to.equal("Dynamic list sample")
    );

    cy.get('[role="columnheader"]').should($header =>
      expect($header).to.have.length(13)
    );

    cy.get('[role="row"]').should($row => expect($row).to.have.length(21));
    cy.get("div[aria-rowindex='1']").should('be.visible')

  });
});
describe("list header", () => {
  before(function() {
    cy.visit("/login");

    //@ts-ignore
    cy.login();
    //
  });

  it("should be expendable", () => {
    cy.visit("/list");
    //cy.log("init", w);
    // this yields us a jquery object

    const initialWidth = 55; //asert element width

    cy.get("div[data-testid='table-header-id']").should($id =>
        expect($id.width()).equal(initialWidth)
      );
      cy.get("div[data-testid='table-header-id']").should($id =>
        expect($id.width()).equal(initialWidth)
      );
    cy.get("div[data-testid='table-header-id'] span.react-draggable")
      .trigger("mousedown", { which: 1 })
      .trigger("mousemove", { clientX: 200, clientY: 0 })
      .trigger("mouseup", { which: 1 });

    cy.get("div[data-testid='table-header-id']").should($id =>
      expect($id.width()).greaterThan(initialWidth)
    );
    cy.get("div[data-testid='table-header-url']").should($url =>
        expect($url.width()).lessThan(initialWidth)
      );

  });
});
describe("list content", () => {
  before(function() {
    cy.visit("/login");

    //@ts-ignore
    cy.login();
    //
  });

  it("should be scrollable", () => {
    cy.visit("/list");
    cy.get("div[role='rowgroup']").scrollTo(0, 500);
    cy.get("div[aria-rowindex='13']").should('be.visible')
  });
});
