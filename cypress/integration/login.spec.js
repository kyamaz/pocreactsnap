/// <reference types="cypress" />
const INVALID_LOG = "erere";
const INVALID_PWD = "rrrrrrrr";
const INVALID_CRED = {
  login: INVALID_LOG,
  pwd: INVALID_PWD
};
const VALID_LOG = "toto";
const VALID_PWD = "rerere";
const VALID_CRED = {
  login: VALID_LOG,
  pwd: VALID_PWD
};
const LOGIN_POST_OPT = {
  method: "POST",
  url: "http://localhost:4500/login",
  form: true,
  failOnStatusCode: false
};

describe("not providing credentials", () => {
  it("should prevent submit", () => {
    cy.visit("/login");
    cy.get("#signin-btn").click();
    cy.get("#login-submit").should("have.attr", "disabled");
  });
});

describe("providing unauthentified credentials", () => {
  it("should prevent redirection to app", () => {
    cy.visit("/login");
    cy.get("#signin-btn").click();
    cy.get("#login-input[type='text']").type(INVALID_LOG);
    cy.get("#pwd-input[type='password']").type(INVALID_PWD);
    cy.get("#login-submit").click();
    ///call api
    //@ts-ignore
    cy.request({
      ...LOGIN_POST_OPT,
      body: INVALID_CRED
    })
      .then(resp => {
        expect(resp.status).to.eq(401);
      })
      .then(res =>
        cy
          .get("#form-hint")
          .should($p => expect($p.text()).to.match(/login failed/))
      );

      cy.location().should($loc => {
        expect($loc.pathname).to.eq("/login");
      });

  });
});
describe("providing authentified credentials", () => {
  it("should redirect to app", () => {
    cy.visit("/login");
    cy.get("#signin-btn").click();
    cy.get("#login-input[type='text']").type(VALID_LOG);
    cy.get("#pwd-input[type='password']").type(VALID_PWD);
    cy.get("#login-submit").click();
    ///call api
    //@ts-ignore
    cy.request({
      ...LOGIN_POST_OPT,
      body: VALID_CRED
    }).then(resp => {
      expect(resp.status).to.eq(200);
      expect(resp.body).to.have.property("token");
      window.localStorage.setItem('hookia_token', resp.body.token)
    });
    //redirect
    cy.location().should($loc => {
      expect($loc.pathname).to.eq("/");
    });
  });
});
